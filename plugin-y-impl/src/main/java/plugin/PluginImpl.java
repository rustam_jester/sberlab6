package plugin;

import service.IPlugin;

import java.io.PrintStream;

public class PluginImpl implements IPlugin {
    public static final String PLUGIN_INFO = "Plugin-Y Ver. 1.01";

    public String getPluginInfo() {
        return PLUGIN_INFO;
    }

    public void writeToStream(PrintStream printStream) {
        printStream.println("Hello from PLUGIN-Y!\nMy Class is "+this.getClass());
    }
}
