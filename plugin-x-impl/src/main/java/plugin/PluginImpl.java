package plugin;

import service.IPlugin;

import java.io.PrintStream;

public class PluginImpl implements IPlugin {
    public static final String PLUGIN_INFO = "Plugin-X Ver. 1.0";

    public String getPluginInfo() {
        return PLUGIN_INFO;
    }

    public void writeToStream(PrintStream printStream) {
        printStream.println("Hello from PLUGIN-X!\nMy Class is "+this.getClass());
    }
}
