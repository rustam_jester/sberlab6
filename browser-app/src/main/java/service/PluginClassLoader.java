package service;

import java.net.URLClassLoader;

public class PluginClassLoader extends ClassLoader{

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        Class<?> cl = findLoadedClass(name);
        if (cl == null){
            try{
                if (this.getParent() != null){
                    cl = this.getParent().loadClass(name);

                    /*выглядит очень костыльно, но только так смог сделать
                    чтобы работало по условию усложненного задания...*/
                    if (!cl.getName().equals("service.IPlugin")){
                        cl = this.getParent().getParent().loadClass(name);
                    }
                }
            } catch (ClassNotFoundException ex){
                cl = findClass(name);
            }
        }
        return cl;
    }
}
