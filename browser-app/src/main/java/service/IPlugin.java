package service;

import java.io.PrintStream;

public interface IPlugin {
    String getPluginInfo();
    void writeToStream(PrintStream printStream);
}
