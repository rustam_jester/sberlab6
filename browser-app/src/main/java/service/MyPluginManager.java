package service;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class MyPluginManager {
    private final String pluginRootDirectory;
    public MyPluginManager(String pluginRootDirectory){
        StringBuilder sb = new StringBuilder(pluginRootDirectory);
        if (!pluginRootDirectory.endsWith("/")){
            sb.append("/");
        }
        this.pluginRootDirectory = sb.toString();
    }

    public IPlugin loadPlugin(String pluginName, String pluginClassName)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
        URL jarUrl = (new File(pluginRootDirectory+pluginName).toURI().toURL());
        ClassLoader cl = new URLClassLoader(new URL[]{jarUrl}, new PluginClassLoader());

        Class<?> pluginClass = cl.loadClass(pluginClassName);
        return (IPlugin) pluginClass.newInstance();
    }

}
