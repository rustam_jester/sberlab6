package plugin;

import service.IPlugin;

import java.io.PrintStream;

public class PluginImpl implements IPlugin {
    public static final String PLUGIN_INFO = "Plugin-Internal Ver. 1.1";

    public String getPluginInfo() {
        return PLUGIN_INFO;
    }

    public void writeToStream(PrintStream printStream) {
        printStream.println("Hello from PLUGIN-INTERNAL!\nMy Class is "+this.getClass());
    }
}
