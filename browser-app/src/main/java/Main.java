import plugin.PluginImpl;
import service.IPlugin;
import service.MyPluginManager;

public class Main {
    private static void printPluginInfo(IPlugin plugin){
        StringBuilder sb = new StringBuilder("Загружен плагин:");
        sb.append("\nClassLoader: ").append(plugin.getClass().getClassLoader());
        sb.append("\nPlugin info: ").append(plugin.getPluginInfo());
        System.out.println(sb.toString());
    }

    private static void pluginOperation(IPlugin plugin){
        plugin.writeToStream(System.out);
        System.out.println();
    }

    public static void main(String[] args) {

        IPlugin internalPlugin = new PluginImpl(); //Загрузка своего плагина
        printPluginInfo(internalPlugin);
        pluginOperation(internalPlugin);

        try {

            MyPluginManager manager = new MyPluginManager("plugin-x-impl/target/");
            IPlugin p = manager.loadPlugin("plugin-x-impl-1.0-SNAPSHOT.jar", "plugin.PluginImpl");

            printPluginInfo(p);
            pluginOperation(p);

            manager = new MyPluginManager("plugin-y-impl/target/");
            p = manager.loadPlugin("plugin-y-impl-1.0-SNAPSHOT.jar", "plugin.PluginImpl");

            printPluginInfo(p);
            pluginOperation(p);

        } catch (Throwable t){
            System.err.println(t.toString());
            if (t.getCause() != null){
                System.err.println(t.getCause().toString());
            }
        }

    }
}
